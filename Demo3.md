## Demo Project:
Install a stateful service (MongoDB) on Kubernetes using Helm
## Technologies used:
K8s, Helm, MongoDB, MongoExpress, Linode LKE, Linux
## Project Decription:
* Create a managed K8s cluster with Linode Kubernetes Engine
* Deploy replicated MongoDB service in LKE cluster using a Helm chart
* Configure data persistence for MongoDB with Linode’s cloud storage
* Deploy UI client  Mongo Express for MongoDB
* Deploy and configure nginx ingress to access the UI application from browser
## Description in details:
### Create a managed K8s cluster with Linode Kubernetes Engine
__Step 1:__ From `Create`tab choose `Kubernetes`
1. Choose region
2. Kubernetes version
3. Choose nodes (choose two nodes 4GB)

__Note:__ Ypu don't have to create master nodes because managed Kubernete service basically __manages__ the master nodes for you. You only need to care about the worker nodes

__Step 2:__ When your nodes ready download `kubeconfig` file to get credentials to connect your nodes

__Step 3:__ Set `kubeconfig.yaml` (by default it calls `test-kubeconfigf.yaml`) file as an environmental variable
```sh
export KUBECONFIG=test-kubeconfigf.yaml
```
__Note:__ If you execute `kubectl get node` you can see your worker nodes

### Deploy replicated MongoDB service in LKE cluster using a Helm chart
>#### Before to start:
>__Step 1:__  Install [Helm](https://helm.sh/docs/intro/install/) using documentation
>
>__Note:__ You can find charts on [artifacthub.io](https://artifacthub.io/)

__Step 1:__ Search  Mongodb Helm Chart ([here](https://github.com/bitnami/charts/tree/main/bitnami/mongodb))

__Step 2:__ Install Helm Chart
```sh
$ helm repo add my-repo https://charts.bitnami.com/bitnami
$ helm install my-release my-repo/mongodb
```
__Important:__ When you execute the Helm command it is going to executed against the cluster that you __conected__  

### Configure data persistence for MongoDB with Linode’s cloud storage
__Step 1:__ Return to Helm Chart [page](https://github.com/bitnami/charts/tree/main/bitnami/mongodb) and see parametres and configure them
1. Create file(`test-mongodb-values.yaml`) that overrrite default parametres

2. Execute command to overrrite values
```sh
helm install mongodb --values test-mongodb-values.yaml bitnami/mongodb
```
__Note:__ command syntax: `helm install [our name] --values [values file name] [chart name]`
__our name__ it's the name that we give

__Note:__ Helm Chart should use the StorageClass of Linode's Cloud Storage

### Deploy UI client  Mongo Express for MongoDB
__Step 1:__ Create Mongo Express file (Use attached file `test-mongo-express.yaml`)

__Note:__ you can see all description variables [here](https://hub.docker.com/_/mongo-express)

__Step 2:__ Apply it to the cluster
```sh
kubectl apply -f test-mongo-express.yaml
```
### Deploy and configure nginx ingress to access the UI application from browser
__Step 1:__ Find Helm chart for ingress controller and install it

__Step 2:__ Ingress controller some cloud native loadbalancer in the background

__Note:__ You can find created balancer that dynamically created and provisioned as you created ingress controller

__Step 3:__ Create ingress rule for a Mongo Express service so that you can access it from browser (You can find complete file `test-ingress.yaml` in folder)
1. Configure `host` in file
__host__ - domain address 

__Step 4:__ Apply that file to the cluster

>#### Note:
>1. Browser: hostname configured in ingress rule
>2. Hostname gets resolved to external IP of NodeBaldncer
>3. Ingress Controller resolved rule and forwards to internal Service of MongoExpress
