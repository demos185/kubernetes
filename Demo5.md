## Demo Project:
Setup Prometheus Monitoring in Kubernetes cluster
## Technologies used:
Kubernetes, Helm, Prometheus
## Project Decription:
* Deploy Prometheus in local Kubernetes cluster using a Helmchart
## Description in details:
### Deploy Prometheus in local Kubernetes cluster using a Helmchart
__Step 1:__ Install Prometheus Helm Chart ([link](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack))

__1.__ Get Helm Repository Info
```sh
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
```
__2.__ Install Helm Chart
```sh
helm install [RELEASE_NAME] prometheus-community/kube-prometheus-stack
```
__3.__  This Chart contains:
- 2 StatefulSets (Prometheus server, Alertmanager)
- 3 Deployments (Prometheus Operator, Grafana, Kube State Metrics)
- 3 ReplicaSets (created by deployment)
- 1 DeamonSet (Node Exporter DaemonSet)

__Note 1:__ DaemonSet runs on every Worker Node 

__Note 2:__ `Node Exporter DaemonSet` connects to Server,     translates Worker Node metrics to Prometheus metrics
- ComfigMaps (confogurations for different parts, managed by operator, how to connect  to default metrics)
- Secrets (for: Grafana, Prometheus, Operator)
- __Includes:__ certificates, username & passwords etc.
- CRDs (extensions of K8s API)

__Pods__: from Deployment and StatefulSets

__Services__: each component has own


__With this chart:__ You have Monitoring stack, Configuration for your K8s cluster that means: Worker Nodes monitored, K8s components monitored

__4.__ You can see how it looks here:
```sh
kubectl port-forward deployment/prometheus-grafana 3000
```
- Deffault user: admin
- Password: Look it in the Chart description (`prom-operator`)