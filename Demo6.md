## Demo Project:
Kubernetes with Production & Security Best Practices
## Technologies used:
Kubernetes, Redis, Linux, LinodeLKE
## Project Decription:
* Create K8s manifests for Deployments and Services for all microservices of an online shop application
* Deploy microservices to Linode’s managed Kubernetes cluster
* Security Best Practices
## Description in details:
### Create K8s manifests for Deployments and Services for all microservices of an online shop application
__Before:__ Take project from [here](https://github.com/nanuchi/microservices-demo)
>__Key information you need to know:__
>- What microservices we are deploying>
>- How they are connected?
>- Any 3rd party Services and DataBases?
>- Which Srevice is accessible for outside the cluster?  
>
>__Note:__ You can get this information from developers because obviously they know how their microservices work and communicate with each other
>
>__Also you need to know:__
>- __Image names__ for each microservice 
>- What __environment variables__ each Microservice expects?
>- On __which port__ each Microservice starts

__Step 1:__ Create Folder  for your configurations and create `.yaml` file from scratch

__Step 2:__ Open you `.yaml` file and paste 'skeleton' a minimal configuration for deployment and service for one configuration

__Step 3:__ In documentation of project you can see that it has 11 microservices so copy and past 10 times below

#### Now you just need fill in that information that we need
__Step 4:__ Email service
1. Change name `xxx` on `emailservice`
2. Add label name `emailservice`
3. Give container name (something generic like `service`)
4. Give image URL 
5. Container port `8080`
6. Target port(in service)  `8080`
7. Service port `50000`

__Note:__ Srevice port can be __same or different__ from Container port

>__Important:__ EmailService expects: _"__PORT__"_ environment variable

8. Add in containers define environment variables usinfg `env` attribute
```yaml
env:
- name: PORT
  value: "8080"
```
__Note:__  Environment `PORT` value equal `container port`  

__Step 5:__ RecommendationService
1. Change name `xxx` on `recommendationservice`
2. Labels and selectors: `recommendationservice`
3. Container name
4. Image URL
5. Container port `8080`
6. Target port(in service) `8080`
7. Service port `8080`
8. Requirement variables (you can see them in docs): `PORT`,`PRODUCT_CATALOG_SERVICE_ADDR`

__Step 6:__ ProductCatalogService
1. Change name on `productcatalogservice`
2. Labels and selectors: `productcatalogservice`
3. Container name
4. Iamge URL
5. Container port `3550`
6. Target port `3550`
7. Service port `3550`
8. Variables: `PORT`

__Step 7:__ PaymentService
1. Change name on `paymentservice`
2. Labels and selectors: `paymentservice`
3. Container name
4. Image URL
5. Container port `50051`
6. Target port `50051`
7. Service port `50051`
8. Variables: `PORT`

__Step 8:__ CurrencyService
1. Change name on `currencyservice`
2. Labels and selectors: `currencyservice`
3. Container name
4. Image URL
5. Container port `7000`
6. Target port `7000`
7. Service port `7000`
8. Variables: `PORT`

__Step 9:__ ShippingService
1. Change name on `shippingservice`
2. Labels and selectors: `shippingservice`
3. Container name
4. Image URL
5. Container port `50051`
6. Target port `50051`
7. Service port `50051`
8. Variables: `PORT`

__Step 10:__ AdService
1. Change name on `adservice`
2. Labels and selectors: `adservice`
3. Container name
4. Image URL
5. Container port `9555`
6. Target port `9555`
7. Service port `9555`
8. Variables: `PORT`

__Step 11:__ CartService
1. Change name on `cartservice`
2. Labels and selectors: `cartservice`
3. Container name
4. Image URL
5. Container port `7070`
6. Target port `7070`
7. Service port `7070`
8. Variables: `PORT`
9. Because this microservice connected to `Redis`, whe have another env variable `REDIS_ADDR`
```yaml
env:
- name: REDIS_ADDR
  value: xxx
```
__Note:__ `value` is unkonw because we assign it when we deploy `redis`

__Step 12:__ Redis 
1. Change name on `redis-cart`
2. Labels and selectors: `redis-cart`
3. Container name `redis`
4. Image URL `redis:alphine`
5. Container port `6379`
6. Target port `6379`
7. Service port `6379`
8. Variables: `PORT` and `REDIS_ADDR`
```yaml
- name: REDIS_ADDR
  value: "redis-cart:6379"
```
__Note:__ Redis __persists__ its data __in memory__ 

__Step 13:__ Create volume for `redis` that called `emptyDir`
>__Note:__ `emptyDir` 
>- initially empty
>- irst created when a Pod is assigned to a Node
>- __Exists as long as the Pod is running__ 
>- Container crashing does NOT remove a Pod from a Node
>- Therefore, __data safe across container craches!__

__1.__ Define volume on `containers` level
```yaml
volumes:
- name: redis-data
  emptyDir: {}
```
__2.__ Mount the volume into container
```yaml
containers:
- name: redis
  image: redis:alpine
  ports:
  - containerPort: 6379
  volumeMounts:
  - mountPath: /data
    name: redis-data
```
__Step 14:__ CheckoutService
1. Change name on `checkoutservice`
2. Labels and selectors: `checkoutservice`
3. Container name 
4. Image URL 
5. Container port `5050`
6. Target port `5050`
7. Service port `5050`
8. Variables: `PORT`
>__Note:__ CheckoutService need to know:
>- Endpoint: Service Name + Service Ports
>- for each Microservice it connects to

9. Define endpoints in environment variables
```yaml
- name: PRODUCT_CATALOG_SERVICE_ADDR
  value: "productcatalogservice:3550"
- name: CURRENCY_SERVICE_ADDR
  value: "currencyservice:7000"
- name: CART_SERVICE_ADDR
  value: "cartservice:7070"
- name: RECOMMENDATION_SERVICE_ADDR
  value: "recommendationservice:8080"
- name: SHIPPING_SERVICE_ADDR
  value: "shippingservice:50051"
- name: CHECKOUT_SERVICE_ADDR
  value: "checkoutservice:5050"
- name: AD_SERVICE_ADDR
  value: "adservice:9555"
```
__Note:__ These are going to be exact environment variable names that the application already expects(check [docs](https://github.com/nanuchi/microservices-demo))

__Step 15:__ Frontend
1. Change name on `frontend`
2. Labels and selectors: `frontend`
3. Container name 
4. Image URL 
5. Container port `8080`
6. Target port `8080`
7. Service port `80`
8. Variables: `PORT`

__Note:__ `Frontend` Service needs the service Endpoints

9. Define endpoints
```yaml
- name: PRODUCT_CATALOG_SERVICE_ADDR
  value: "productcatalogservice:3550"
- name: CURRENCY_SERVICE_ADDR
  value: "currencyservice:7000"
- name: CART_SERVICE_ADDR
  value: "cartservice:7070"
- name: RECOMMENDATION_SERVICE_ADDR
  value: "recommendationservice:8080"
- name: SHIPPING_SERVICE_ADDR
  value: "shippingservice:50051"
- name: CHECKOUT_SERVICE_ADDR
  value: "checkoutservice:5050"
- name: AD_SERVICE_ADDR
  value: "adservice:9555"
```

10. Create exnternal service with `type: NodePort` and need to assign `nodePort`
>__Note:__ 
>- ClusteIP is an internsal Service
>- Fontend needs __external__ access
```yaml
apiVersion: v1
kind: Service
metadata:
  name: frontend-external
spec:
  type: NodePort
  selector:
    app: frontend
  ports:
  - name: http
    port: 80
    targetPort: 8080
    nodePort: 30007
```
### Deploy microservices to Linode’s managed Kubernetes cluster
__Step 1__ Create Kubernetes cluster in Linode with 3 small nodes(2GB)

__Step 2:__ Download kubeconfig file and change permissions
```ch
chmod 400 ~/PATH_TO_YOUR_KUBECONFIG.yaml
```
__Step 3:__ Export kube config variable to point to the online shop microservices config file from linode
```sh
export KUBECONFIG=~/PATH_TO_YOUR_KUBECONFIG.yaml
```
__Step 4:__ Deploy microservices

__1.__ Create namespace `microservices`
```sh
kubectl create ns microservices
```
__2.__ Deploy microservices
```sh
kubectl apply -f config.yaml -n microservices
```
### Security Best Practices:

__Best Practice 1:__ Pinned (Tag) Version for each Container image

__1.__ Specify a __pinned version__ in each container name 

__Good:__
```yaml
spec:
  containers:
  - name: app
    image: nginx:1.19.8
```
__Bad:__
```yaml
spec:
  containers:
  - name: app
    image: nginx
```
>__Note:__ If you don't specify version kubernetes will pull last version of image that could lead problems

__2.__ Fixate container image for each microservice
- Either ask which image vevrsion needs to be deployed
- We will know when we handle CI/CD pipeline

__Best Practice 2:__ Liveness Probe for each container
> __Liveness Probe__ is health check of containers inside pods. Without it Kubernetes couldn't notice this problem and continue run this Pods. With __Liveness Probe__ you can tell Kubernetes about this problems

__1.__ We could add this simple script ourselves (in this demo we have program gor this)
```yaml 
containers:
  livenessProbe:
    periodSeconds: 5
    exec:
      command: ["/bin/grpc_health_probe", "-addr=:8080"]
```
__Note 1:__ It's contaier attribute
__Note 2:__ We define period when checks, and execute command 

__2.__ Add this probe  to every microservice. __Don't forget__ change port!

__Best Practice 3:__ Readiness Probe for each container
>__Readiness Probe__ checks how applications starts and ready to requests. Example: If your app needs 2min to be ready, without this probe you will have bunch of errors in your applications

__1.__ Add this probe to your containers (Similar to `Liveness Probe`)
```yaml
readinessProbe:
  periodSeconds: 5
  exec:
    command: ["/bin/grpc_health_probe", "-addr=:8080"]
```
__Note:__ Kuebernetes makes probe connection at the node, not in the pod

__Best Practice 4:__ Resource Request for each container

__Resource Request__ used for define how much resources container can use 

__1.__ Define `Resource Request`
```yaml
resources:
  requests:
    cpu: 100m
    memory: 64Mi
```
>__Note 1:__ This section created in `containers` like 'probs'
>
>__Note 2:__ You can define minimal resources for container

>__CPU__ resources are defined in __millicores__
>
>__Memory__ resources are defined in __bytes__


__Best Practice 5:__ Resource Limits

__1.__ Define `Resource Limits`
```yaml
  limits:
    cpu: 200m
    memory: 128Mi
```
>__Note 1:__ This section created in `containers` like 'probs'
>
>__Note 2:__ You can define limit resources for container
>
>__Note 3:__ If you put __values larger than your biggest node__, your Pod will __never be scheduled!__

>__CPU__ resources are defined in __millicores__
>
>__Memory__ resources are defined in __bytes__

__Best Practice 6:__ _DON'T_ expose a Nodeport!
>__Why it's a Bad practice?__
>It exposes cluster to a security risk. Because it opens a port on all worker nodes in the cluster where it can be directly accessed by external sources. So you have multiple entry points in the cluster and you increase the attack surface. The __Best Practice__ is to only use internal services and have one entry point to the cluster where all requests come in. Ideally that entry point should be sitting outside the cluster on a separate esrver

__1.__ Instead `NodePort` you can use `LoadBalancer` type which uses the cloud platforms load balancer to create an external single entry point for the cluster

__Alternative__ As an alternative  you can also use ingress controller to direct traffic to the internal services
 
__Best Practice 7:__ More than 1 Replica for Deployment
>__Note:__ When you don't configure the replicas it's 1 __by default!__

>__Why it's important__ 
>- If 1 Pod crashes, your apllications is __not accessible__ until new Pod restarts!
>- With more replicas, the application is __always accessible. No downtime__ for users!

> __Always deploy more than 1 replica__ of each application/microservice

__1.__  Add `replicas:` in `spec` section
```yaml
spec:
  replicas: 2
```

__Best Practice 8:__ More that 1 Worker Node in your cluster
>__Why it's important?__
>- __Single Point of Failure__ with just 1 Node
>- You need to __replicate your Nodes__ as well
>- Each replica should run on a different Node

>__Reasons for Server Unavailability__
>- Server crahes
>- Server reboots, because of some update
>- Server maintenance
>- Server broken

__Best Practice 9:__ Using Labels
>__Labels:__
>- Labels are __Key Pairs__
>- Attach to K8s resources

>__Why it's important?__
>- __Custom identifier for your components__
>- Should be __meaningful and relevant to users__
>- With these labels they __can be referced__

>__Use for:__
>1. Groups Pods with labels
>2. Reference in Service Component

__Best Practice 10:__ Using Namespaces
>__Why it's important?__
>- __Organize__ K8s resources in Namespace

>__With Namespaces you also can:__
>- Define __Access Rights based on Namespaces__

#### 3 Security Best Practices
__1.__ Ensure __Images__ are __free of vulnerabilities__
- Third-Party Libraries with vulnerabilities
- Base images with vulnerabilities

__2.__ __No Root access__ for users
>__Why it's important?__
>- With root access they have __access to host-level resources__
>- If container is hacked, much more damage can be done!

{+ Configure Containers to use unprivileged users +}

__Note:__ Most official images do not use root user

__Good practice:__ _Check whether container is running as root_ 

__3.__ __Update Kubernetes__ to the __latest version__
>__Why it's important?__
> {+ Important __Security Fixes__  +}
> 
> {+ General __Bug Fixes__ +}

__Important:__ Update K8s version __Node by Node__
- {+ Avoid Application Downtime +}



