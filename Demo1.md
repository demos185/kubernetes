## Demo Project:
Deploy MongoDB and Mongo Express into local K8s cluster
## Technologies used:
Kubernetes, Docker, MongoDB, MongoExpress
## Project Decription:
* Setup local K8s cluster with Minikube
* Deploy MongoDB and Mongo Express with configuration and credentials extracted into ConfigMap and Secret
## Description in details:
### Setup local K8s cluster with Minikube
__Step 1:__ Install minikube ([guide here](https://minikube.sigs.k8s.io/docs/start/))
```sh
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
sudo dpkg -i minikube_latest_amd64.deb
```
__Step 2:__ Start minikube (via Docker)
```sh
minikube start --driver docker
```
_Note: Minikube has __Docker pre-installed__ to run the containers in the cluster_

> ### Important:
>  __By default__, kubectl gets configured to access the kubernetes cluster control plane inside minikube when the minikube start command is executed.
> __However__ if kubectl is not installed locally, minikube already includes kubectl which can be used like this:
>```sh
>minikube kubectl -- <kubectl commands>
>```
>You can also alias kubectl for easier usage.
>```sh
>alias kubectl="minikube kubectl --"
>```
>Alternatively, you can create a symbolic link to minikube’s binary named ‘kubectl’.
>```sh
>ln -s $(which minikube) /usr/local/bin/kubectl
>```
>

### Deploy MongoDB and Mongo Express with configuration and credentials extracted into ConfigMap and Secret
#### MongoDB
__Step 1:__ Create MongoDB deployment file
1. Create blueprint for MongoDB

__Note:__ you can take all blueprints for deployment, secret, service e.t.c. in [official documintation](https://kubernetes.io/docs/home/)

2. Open [Image page](https://hub.docker.com/_/mongo) and look how to use this container 
3. Specify ports (`-containerport: 27017`)
4. Specify nessesary environments (create vriables for it)
5. Create Secret for environments (`mongo-secret.yaml`)
```yaml
apiVersion: v1
kind: Secret
metadata:
    name: mongodb-secret
type: Opaque
data:
    mongo-root-username: 
    mongo-root-password:
```
__`type: Opaque`__ - "Opaque" - default name for arbitary key-value pairs
>__IMPORTANT__ 
>Storing the data in a Secret component doesn't automatically make it secure. There are the built-in mechanism (like encryption) for basic security, whick are not enabled by default

__6.__ create values for user name and pwd:
```sh
echo -n 'username_here' | base64
```
__IMPORTANT:__ Secret must be created __before__ the Deployment

__Step 2:__ Apply crated secret
```sh
kubectl apply -f mongo-secret.yaml
```
__Note:__ Secret can be referenced now in Deployment

__Step 3:__ Return to deployment file and assign secret
```yaml
env:
- name: MONGO_INITDB_ROOT_USERNAME
  valueFrom:
    secretKeyRef:
      name: mongodb-secret
      key: mongo-root-username
- name: MONGO_INITDB_ROOT_PASSWORD
  valueFrom: 
    secretKeyRef:
      name: mongodb-secret
      key: mongo-root-password
```
__Step 4:__ Apply deployment
```sh
kubectl apply -f mongo.yaml
```
__Step 5:__ Create Service for `MongoDB`
__Note:__ We can create it inside Deployment file just divide them using `---` (Deployment and Service in 1 file, because they belong together) 

__Step 6:__ Apply service using the same command
```sh
kubectl apply -f mongo.yaml
```

#### Mongo Express
__Step 1:__ Create deployment for Mongo Express

__Note:__ You can take all blueprints for deployment, secret, service e.t.c. in [official documintation](https://kubernetes.io/docs/home/)

1. Open [Image page](https://hub.docker.com/_/mongo-express) and look how to use this container 

2. Specify ports (`-containerport: 8081`)

3. Add environments for Mongo Express (admin-username, admin-pwd, mongodb-server)
```yaml
env:
- name: ME_CONFIG_MONGODB_ADMINUSERNAME
  valueFrom:
    secretKeyRef:
      name: mongodb-secret
      key: mongo-root-username
- name: ME_CONFIG_MONGODB_ADMINPASSWORD
  valueFrom: 
    secretKeyRef:
      name: mongodb-secret
      key: mongo-root-password
- name: ME_CONFIG_MONGODB_SERVER
  valueFrom: 
    configMapKeyRef:
      name: 
      key: 
```
__Note 1:__ Pay attention that we use the same environment from `MongoDB`

__Note 2:__ `ME_CONFIG_MONGODB_SERVER` can be used by different components so we gonna create configmap that will be called `mongodb-configmap`

__Step 2:__ Create configmap for deployment

1. Create configmap
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: mongodb-configmap
data:
  database_url: mongodb-service
```
__`data`__ - the actual contents in key-value pairs 

__IMPORTANT:__ Configmap must __already__ be in the k8s cluster, when referencing it!

2. Return to deployment and add missing information
```yaml
- name: ME_CONFIG_MONGODB_SERVER
  valueFrom: 
    configMapKeyRef:
      name: mongodb-configmap
      key: database_url
```
__Step 3:__ Apply created files to cluster

```sh
kubectl apply -f mongo-configmap.yaml
kubectl apply -f mongo-express.yaml
```
#### Mongo Express External Service
__Step 1:__ Create service inside deployment like in `MongoDB` example

__Step 2:__ Make it External service
1. add `type` incide `spec` and define it `LoadBalancer`

2. Add third port in ports that called `nodePort` (Port for external IP adress)

__`-nodePort`__ - must be between 30000-32767
```yaml
spec:
  selector:
    app: mongo-express
  type: LoadBalancer  
  ports:
    - protocol: TCP
      port: 8081
      targetPort: 8081
      nodePort: 30000
```
3.  Apply created service
```sh
kubectl apply -f mongo-express.yaml 
```
__Step 3:__ Make cluster available (__In minikube!__)
```sh
minikuybe service mongo-express-service
```
__Note:__ It will opet tab in your browser with mongo express page