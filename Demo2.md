## Demo Project:
Deploy Mosquitto message broker with ConfigMap and Secret Volume Types
## Technologies used:
Kubernetes, Docker, Mosquitto
## Project Decription:
* Define configuration and passwords for Mosquitto message broker with ConfigMap and Secret Volume types
## Description in details:
### Define configuration and passwords for Mosquitto message broker with ConfigMap and Secret Volume types
>#### Configmap and Secret for mounting files
>
>
>
>
__Step 1:__ Create configmap like `MongoDB` example for [Mosquito](https://hub.docker.com/_/eclipse-mosquitto)
__1.__ Instead of creating file with idividual values we create another file that  define the name of file with content of that file
```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: mosquitto-config-file
data:
  mosquitto.conf: |
    log_dest stdout
    log_type all
    log_timestamp true
    listener 9001
```
__Note 1:__ Example `MongoDB` use individual values

__Note 2:__ With configmap and secret you can create individual key pairs but you can also create files that __can be mounted__ into the pod and then into the container!

__Step 2:__ The same way we create secret file 
```yaml
apiVersion: v1
kind: Secret
metadata:
  name: mosquitto-secret-file
type: Opaque
data:
  secret.file: |
    c29tZXN1cGVyc2VjcmV0IGZpbGUgY29udGVudHMgbm9ib2R5IHNob3VsZCBzZWU=
```
__Note 1:__ Inside we use encryption like in `MongoDB` example

__Note 2:__ Sometime you need client certifications for your services so that they can communicate with other secured services - this way you can create a separate component(`secret`) where let's say you define a CI certificate or client certificate file and here just paste in the base64 encoded of that  certificate file

__Step 3:__ Create Deployment for `mosquitto` without volumes (just to see how it works) and with volumes
#### Note: You can find every exaple in `mosquitto`  folder
__Important:__ Configmap and Secret must be created and exist __before__ Pod starts in the kubernetes cluster 

__Summorize:__ You can run those deployments and see the difference between them