## Demo Project:
Deploy our web application in K8s cluster from private Docker registry
## Technologies used:
Kubernetes, Helm, AWS ECR, Docker
## Project Decription:
* Create Secret for credentials for the private Docker registry
* Configure the Docker registry secret in application Deployment component
* Deploy web application image from our private Docker registry in K8s cluster
## Description in details:
### Create Secret for credentials for the private Docker registry
>__Before:__ Execute `docker login`command to your private repo (It could be AWS private repository or dockerhub etc.)
>
>__Note:__ If You use minikube for this demo, you will need to SSH into minikube and execute `docker login` command inside minikube because minikube has own docker inside the container 

__Step 1:__ Create secret which will have the access token of credentials to your private repository which will allow docker to pull your image
```yaml
apiVesion: v1
kind: Secret
metadata:
    name: my-registry-key
data:
    .dockerconfigjsond:
type: kubernetes.io/dockerconfigjson
```
__`dockerconfigjson:`__ here you must put base64 encoding content from created `./docker/config.json` file 

__Or__ You can create secret with encryption using this command:
```sh
kubectl create secret generic my-registry-key \
--from-file=.dockerconfigjson=.docker/config.json \
--type=kubernetes.io/dockerconfigjson
```
__Alternative:__ You can execute both commands using this command:
```sh
kubectl create secret docker-registry my-registry-key-two \
--docker-server=LINK_TO_YOUR_PRIVATE_REPOSITORY \
--docker-username=YOUR_USERNAME_IN_PRIVATE_REPOSITORY
--docker-password=YPOUR_PASSWORD_FOR_DOCKER_REGISTRY
```
__my-registry-key-two__  - It's name of secret
>__Note:__ With this command you can only create 1 secret that  has access token for one docker registry. In another usecases where you have more then 1 private repository - using first variant will be more practical and more convenient 
### Configure the Docker registry secret in application Deployment component
__Step 1:__ Create Deployment component (`my-app-deployment.yaml`)

__1.__ Image name of the application has to be the complete name which includes the repository URL and image name 
```yaml
image: repository/image_name:version
```
__`imagePullPolicy: always`__ Every time pod is created this will force docker to re-pull the image even if it already exist locally on your local host

__2.__ Configure attribute which called `imagePullSecrets`
```yaml
imagePullSecrets: 
- name: my-registry-key
```

__Step 2:__ Apply created configurations

__Important:__ Secret must be in __same namespace__ as Deployment
### Deploy web application image from our private Docker registry in K8s cluster
__Step 1:__ Add created deployment into your cluster