## DemoProject:
Deploy Microservices with Helmfile
## Technologiesused:
Kubernetes, Helm, Helmfile
## ProjectDecription:
* Deploy Microservices with Helm
* Deploy Microservices with Helmfile
## Description in details:
### Deploy Microservices with Helm
You can write script for installing commands and another for uninstalling commands

>__Note:__ It's not efficient way 

### Deploy Microservices with Helmfile
`Helmfile` - __Declarative way__ for deploying helm charts (Define the __desired__ state!)
- Declare a definition of an entire K8s cluster
- Change specification dependiong on application or type of environmnt

__Step 1:__  Create `helmfile.yaml`

__1.__ Configure all your services in here

__2.__ Define `releases` attribute on top level and under which  you can define  a list of all releases that you are deploying into the cluster
```yaml
releases:
  - name: rediscart
    chart: charts/redis
    values: 
    - values/redis-values.yaml 
```
__3.__ Do the same for another realises

__Note:__ You can override any idividual value that is defined in the values file very simply like this
```yaml
releases:
  - name: rediscart
    chart: charts/redis
    values: 
    - values/redis-values.yaml 
    - appReplicas: "1"
```

__Step 2:__ Install Helmfile tool ([Documentation](https://github.com/helmfile/helmfile))

__Step 3:__ Deploy Helm Charts

__1.__ Use `helmfile` command to update the cluster or sink the cluster with whatever we have declared in our helm file as a desired state for our applications
```ssh
helmfile sync
```

__Uninstall chart:__ Use `helmfile destroy` command