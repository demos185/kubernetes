## List of Kubernetes Demos:

__1.__ Deploy MongoDB and Mongo Express into local K8s cluster

__2.__  Deploy Mosquitto message broker with ConfigMap and Secret Volume Types

__3.__ Install a stateful service (MongoDB) on Kubernetes using Helm

__4.__ Deploy our web application in K8s cluster from private Docker registry
 
__5.__ Setup Prometheus Monitoring in Kubernetes cluster

__6.__ Deploy Microservices application in Kubernetes with Production & Security Best Practices
 
__7.__ Create HelmChart for Microservices
  
__8.__ Deploy Microservices with Helmfile

## Folders with code for Demos:

__Mongo:__ Demo#1

__Mosquitto:__ Demo#2

__Linode-Kubernetes-Engine:__ Demo#3

__K8s-project:__ Demo#4

__Online-shop__: Demo#6

__Helm-chart__: Demo#7, Demo#8