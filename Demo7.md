## Demo Project:
Create HelmChart for Microservices
## Technologies used:
Kubernetes, Helm
## Project Decription:
* Create 1 shared HelmChart for all microservices, to reuse common Deployment and Service configurations for the services
## Description in details:
### Create 1 shared HelmChart for all microservices, to reuse common Deployment and Service configurations for the services
__Step 1:__ Create chart
```sh
helm create microservice
```
__Note:__ It will create folder with some configurations:
- `Chart.yaml` = meta info about chart
- `charts` folder = chart dependencies
- `.helmignore` = files you don't want to include in your helm chart
- `templates` foler = where the atual K8s YAML files (template files) are created
- `values.yaml` = actual values for the template files (Clean this file)

__Note 1:__ In this Demo  you will create templates from scratch so you don't need files in `templates` folder except `deployment` and `service`(remove contents inside them)

__Step 2:__ Create Basic Template files
1. Create blueprints for deployment and service
2. Create variables for name, container port, service port, image, image version, replicas 

__Note:__ To set variables in template file, use the syntax `{{ .values.VARIABLE_NAME }}` (where `VARIABLE_NAME` name of your variable)

>"Values" Object:
>1. A built-in object
>2. By default, _Values_ is empty
>3. Values are passed into teamplate from __3 sources:__
>    - the _values.yaml_ file in the chart
>    - user-suplied file _passed_ with -f flag
>    - parameter _passed_ with `--set` flag

>Built-in objects
>- Several objects are passed into a template from the template engine
>- Examples: "Realise", "files", "Values" ...
>- You can read about it [here](https://helm.sh/docs/chart_template_guide/builtin_objects/)

>Variable Naming Convertions:
>- Names should begin with a lowercase letter
>- Separated with camelcase

__Note:__ If you want to reference to variable like "IMAGE_NAME:IMAGE_VERSION" you can do this using this syntax(__enclose__ into double quotes):
```yaml
image: "{{ .values.your_image_variable }}:{{ .value.image_version }}"
```
__Flat on Nested values__
- Values may be flat or nested already
- Best practice it to use flat structure, which parameter
>Example:
>```yaml
>appName: myapp
>Appreplicas: 1
>```

3. Dynamic Environment Variables

For this environment you can use ranges with this syntax:
```yaml
env:
{{- range .Values.containerEnvVars}}
- name: {{.key}}
  value: {{.value | quote }}
{{- end}}  
```
__Note 1:__ The Environment variable values are always interpreted as strings! Use pipe and `quote` to interpret

__Note 2:__  To close loop use `{{-end}}`

__Step 3.__ Set Values

1. For this task create file `values.yaml` for using it as blueprint for microservices


Example:
```yaml 
appName: servicename
appImage: gcr.io/google-samples/microservices-demo/servicename
appVersion: v.0.0.0
appReplicas: 1
containerPort: 8080
containerEnvVars:
- name: ENV_VAR_ONE
  value: "valueone"
- name: ENV_VAR_TWO
  value: "valuetwo"  

servicePort: 8080
serviceType: ClusterIP
```
__Note:__ You can also add service variables here


2. Now you can use blueprint for each service like `email-service` 

>__How check that you are producing valid K8s yaml file:__
>
>Use hel command for that 
>```yaml
>helm template -f email-service-values.yaml microservice
>```
>__Note 1:__ `helm template` = render chart templates locally and display the output
>
>__Syntax:__ helm template -f YOUR_VALUE_FILE CHART_NAME 
>

>You can also use `Helm lint` command (syntax the same like `helm template`)
>- examines a char for possible issues
>
>`ERROR` = issuees that will cause chart  to fail installation
>
>`WARNING` = issues that break with convention or recommendations

__To install your chart__ Use `helm install` command
```sh
helm install -f myvalues.yaml emailservice microservice
```
`helm install` = install a chart 

`-f myvalues.yaml` = override values from a file

`emailservice` = release name

`microservice` = chart name

3. Create value files for another services coping `email-service-value.yaml` and chage values inside them

__Note:__ Don't do the same with Redis because it's third-party service that you not develop yourself. 

__Step 5.__ Create Redis Helm chart for the redis-cart service

1. Create another folder for this chart
```sh
helm create redis
```
2. Clean this folder like previous exaple and create `deployment.yaml` and `service.yaml` file 

3. Create `values` file 

4. Create `redis-values` file in the same place where you have your service value files for overriding 


